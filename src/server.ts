import { assertConfigIsValid, config } from "./config";
import "source-map-support/register";
import express from "express";
import cors, { CorsOptions } from "cors";
import pinoHttp from "pino-http";
import crypto from "crypto";
import geoip from "geoip-lite";
import axios from 'axios';
import { simpleGit } from "simple-git";
import { logger } from "./logger";
import { keyPool } from "./key-management";
import { adminRouter } from "./admin/routes";
import { proxyRouter, rewriteTavernRequests } from "./proxy/routes";
import { handleInfoPage } from "./info-page";
import { logQueue } from "./prompt-logging";
import { start as startRequestQueue } from "./proxy/queue";
import { init as initUserStore } from "./proxy/auth/user-store";

const PORT = config.port;

const app = express();

const ACCESS_DENIED_HTML = `<!DOCTYPE html><html lang="en"><head><style> body{margin:0 !important;padding:0px !important;overflow-x:hidden!important;background-size:cover !important;background-color:#222 !important;background-image:url(https://lainchan.org/static/404_background.gif);background-repeat:no-repeat !important;background-position:0!important;background-attachment:fixed !important;background-size:cover !important}</style></head></html>`
const INTERNAL_ERROR_HTML = `<!DOCTYPE html><html lang="en"><head><style> body{margin:0 !important;padding:0px !important;overflow-x:hidden!important;background-size:cover !important;background-color:#222 !important;background-image:url(https://lainchan.org/static/404_background.gif);background-repeat:no-repeat !important;background-position:0!important;background-attachment:fixed !important;background-size:cover !important}</style></head></html>`
const NOT_FOUND_HTML = `<!DOCTYPE html><html lang="en"><head><style> body{margin:0 !important;padding:0px !important;overflow-x:hidden!important;background-size:cover !important;background-color:#222 !important;background-image:url(https://lainchan.org/static/404_background.gif);background-repeat:no-repeat !important;background-position:0!important;background-attachment:fixed !important;background-size:cover !important}</style></head></html>`;
const INTERVAL_TIME = 60000; // Config check interval time in milliseconds (e.g., 60000 ms = 1 minute)

let niggerList: string[] = [];

// Define a custom CORS policy // NEVER USE THIS 
const corsOptions: CorsOptions = {
  origin: (origin, callback) => {
    if (origin === "https://alwaysfindtheway.github.io") {
      // Block requests from the specified domain
      callback(new Error("CORS_ERROR"));
    } else {
      // Allow requests from all other domains
      callback(null, true);
    }
  },
};

interface VpnIpInfo {
  ip: string;
  subnetMask?: number;
}

const vpnIps = new Map<string, VpnIpInfo>(); 
const vpnListUrl = 'https://files.catbox.moe/d88mni.txt';
axios.get(vpnListUrl)
  .then(response => {
    const ips = response.data.trim().split('\n');
    for (const ip of ips) {
      let subnetMask;
      const parts = ip.split('/');
      if (parts.length === 2) {
        subnetMask = parseInt(parts[1]);
      }
      const newVpnIpInfo: VpnIpInfo = {
        ip: parts[0],
        subnetMask: subnetMask
      };
      vpnIps.set(newVpnIpInfo.ip, newVpnIpInfo);
    }
    console.log(`Loaded ${ips.length} VPN IPs.`);
  })
  .catch((error: any) => {
    console.error('Failed to load VPN IP list:', error);
  });
  

  function isVpnIp(req: express.Request): boolean {
    // Add your server IP here
    const serverIp = req.connection.remoteAddress;
    const ip = req.ip;
  
    // Exclude the server IP from the VPN IP check
    if (ip === serverIp) {
      return false;
    }
  
    const vpnIpInfo = vpnIps.get(ip);
    if (vpnIpInfo) {
      if (vpnIpInfo.subnetMask) {
        const [vpnIpAddr, vpnIpMask] = vpnIpInfo.ip.split('/');
        const [reqIpAddr, reqIpMask] = ip.split('/');
        const vpnIpLong = ipToLong(vpnIpAddr);
        const reqIpLong = ipToLong(reqIpAddr);
        const subnetMaskBits = 32 - vpnIpInfo.subnetMask;
        const mask = ~(2 ** subnetMaskBits - 1);
        const maskedVpnIp = vpnIpLong & mask;
        const maskedReqIp = reqIpLong & mask;
        if (maskedVpnIp === maskedReqIp) {
          return true;
        }
      } else {
        return true;
      }
    }
    return false;
  }
  

function blockVpnIps(req: express.Request, res: express.Response, next: express.NextFunction) {
  if (config.blockVPNs === true && isVpnIp(req)) {
    logger.warn(`Blocked request from VPN IP: ${req.ip}`);
    res.status(403).send(ACCESS_DENIED_HTML);
  } else {
    next();
  }
}

function ipToLong(ip: string): number {
  const parts = ip.split('.');
  const long = (parseInt(parts[0]) << 24) |
               (parseInt(parts[1]) << 16) |
               (parseInt(parts[2]) << 8) |
               parseInt(parts[3]);
  return long >>> 0;
}

const blockOtherIPs = (req: express.Request, res: express.Response, next: express.NextFunction) => {
  const expectedHash = "cf89dd82b4a90709de55ec93dbd85471dbbb0c0e7601c5724fbeb00cb92f5f13";
  const incomingHash = crypto.createHash("sha256").update(req.ip).digest("hex");
  if (incomingHash !== expectedHash && config.blockOtherIPs === true) {
    res.status(403).send(ACCESS_DENIED_HTML);
  } else {
    next();
  }
}

const blockSpecifiedCountryCodes = (req: express.Request, res: express.Response, next: express.NextFunction) => {
  const ip = req.ip;
  const geo = geoip.lookup(ip);

  const countryCodeWhitelist = config.countryCodeWhitelist || ''; 
  const countryCodeWhitelistArray = countryCodeWhitelist ? countryCodeWhitelist.split('/') : [];

  const countryCodeBlacklist = config.countryCodeBlacklist || ''; 
  const countryCodeBlacklistArray = countryCodeBlacklist ? countryCodeBlacklist.split('/') : [];

  const blockMode = config.countryCodeBlockMode ? config.countryCodeBlockMode.toString() : 'none';

  if (blockMode !== "none") {
    if (blockMode === "whitelist" && countryCodeWhitelist && geo && !countryCodeWhitelistArray.includes(geo.country)) {
      logger.warn(`Blocked request from IP attached to a non-whitelisted country: ${ip}`);
      res.status(403).send(ACCESS_DENIED_HTML);
    } else if (blockMode === "blacklist" && countryCodeBlacklist && geo && countryCodeBlacklistArray.includes(geo.country)) {
      logger.warn(`Blocked request from IP attached to a blacklisted country: ${ip}`);
      res.status(403).send(ACCESS_DENIED_HTML);
    } else {
      next();
    }
  } else {
    next();
  }
}

app.use(blockSpecifiedCountryCodes);
app.use(blockOtherIPs);
app.use(blockVpnIps);

/* app.use((req, res, next) => { // Just to stop the discordniggers, venusniggers, redditroons, and leeches
  const referer = req.headers['referer'] || req.headers['origin'] || "";
  if (niggerList.includes(req.ip)) {
    return res.status(429).json({
      error: {
        type: "proxy_rate_limited",
        message: `Kill redditors and discordniggers. Behead redditors and discordniggers. Roundhouse kick a redditor and a discordnigger into the concrete. Crucify filthy redditors and discordniggers. Defecate in a redditors and discordniggers food. Launch redditors and discordniggers into the sun. Stir fry redditors and discordniggers in a wok. Toss redditors and discordniggers into active volcanoes. Urinate into a redditors and discordniggers gas tank. Judo throw redditors and discordniggers into a wood chipper. Twist redditors and discordniggers heads off. Report redditors and discordniggers to the IRS. Karate chop redditors and discordniggers in half. Curb stomp pregnant black redditors and discordniggers. Trap redditors and discordniggers in quicksand. Crush redditors and discordniggers in the trash compactor. Liquefy redditors and discordniggers in a vat of acid. Eat redditors and discordniggers. Dissect redditors and discordniggers. Exterminate redditors and discordniggers in the gas chamber. Stomp nigger skulls with steel toed boots. Cremate redditors and discordniggers in the oven. Lobotomize redditors and discordniggers. Mandatory abortions for redditors and discordniggers. Drown redditors and discordniggers in fried chicken grease. Vaporize redditors and discordniggers with a ray gun. Kick old redditors and discordniggers down the stairs. Feed redditors and discordniggers to alligators. Slice redditors and discordniggers with a katana. `,
      },
    });
  } else if (referer.includes("github.io")) {
    return res.redirect(301, "https://moxxie-nolastname-not-moxxie-proxy.hf.space/");
  } else if (referer.includes("venusai")) {
    niggerList.push(req.ip);
    logger.info(niggerList.toString());
    return res.status(429).json({
      error: {
        type: "proxy_rate_limited",
        //message: `Your IP address is ${req.ip}. All of your requests have been logged, and you have been added to a permanent blacklist. This is not a hacker. None of these messages are from hackers. These are exclusive proxies, meant specifically for OUR community. None of you are welcome. Oh, and: Kill niggers. Behead niggers. Roundhouse kick a nigger into the concrete. Slam dunk a nigger baby into the trashcan. Crucify filthy blacks. Defecate in a niggers food. Launch niggers into the sun. Stir fry niggers in a wok. Toss niggers into active volcanoes. Urinate into a niggers gas tank. Judo throw niggers into a wood chipper. Twist niggers heads off. Report niggers to the IRS. Karate chop niggers in half. Curb stomp pregnant black niggers. Trap niggers in quicksand. Crush niggers in the trash compactor. Liquefy niggers in a vat of acid. Eat niggers. Dissect niggers. Exterminate niggers in the gas chamber. Stomp nigger skulls with steel toed boots. Cremate niggers in the oven. Lobotomize niggers. Mandatory abortions for niggers. Grind nigger fetuses in the garbage disposal. Drown niggers in fried chicken grease. Vaporize niggers with a ray gun. Kick old niggers down the stairs. Feed niggers to alligators. Slice niggers with a katana.`,
        message: `Kill redditors and discordniggers. Behead redditors and discordniggers. Roundhouse kick a redditor and a discordnigger into the concrete. Crucify filthy redditors and discordniggers. Defecate in a redditors and discordniggers food. Launch redditors and discordniggers into the sun. Stir fry redditors and discordniggers in a wok. Toss redditors and discordniggers into active volcanoes. Urinate into a redditors and discordniggers gas tank. Judo throw redditors and discordniggers into a wood chipper. Twist redditors and discordniggers heads off. Report redditors and discordniggers to the IRS. Karate chop redditors and discordniggers in half. Curb stomp pregnant black redditors and discordniggers. Trap redditors and discordniggers in quicksand. Crush redditors and discordniggers in the trash compactor. Liquefy redditors and discordniggers in a vat of acid. Eat redditors and discordniggers. Dissect redditors and discordniggers. Exterminate redditors and discordniggers in the gas chamber. Stomp nigger skulls with steel toed boots. Cremate redditors and discordniggers in the oven. Lobotomize redditors and discordniggers. Mandatory abortions for redditors and discordniggers. Drown redditors and discordniggers in fried chicken grease. Vaporize redditors and discordniggers with a ray gun. Kick old redditors and discordniggers down the stairs. Feed redditors and discordniggers to alligators. Slice redditors and discordniggers with a katana. `,
      },
    });
  } else {
    next();
  }
}); */

app.use("/", rewriteTavernRequests);
app.use(
  pinoHttp({
    quietReqLogger: true,
    logger,
    // SillyTavern spams the hell out of this endpoint so don't log it
    autoLogging: { ignore: (req) => req.url === "/proxy/kobold/api/v1/model" },
    redact: {
      paths: [
        "req.headers.cookie",
        'res.headers["set-cookie"]',
        //"req.headers.authorization",
        //'req.headers["x-forwarded-for"]', // IPs are redacted from the container logs.
        //'req.headers["x-real-ip"]', // IPs are redacted from the container logs.
      ],
      censor: "********",
    },
  })
);
app.use((req, _res, next) => {
  req.startTime = Date.now();
  req.retryCount = 0;
  next();
});
app.use(cors());
app.use(
  express.json({ limit: "10mb" }),
  express.urlencoded({ extended: true, limit: "10mb" })
);
// TODO: this works if we're always being deployed to Huggingface but if users
// deploy this somewhere without a load balancer then incoming requests can
// spoof the X-Forwarded-For header and bypass the rate limiting.
app.set("trust proxy", true);
// routes
app.get("/", handleInfoPage);
app.use("/admin", adminRouter);
app.use("/proxy", proxyRouter);
// 500, 404 and 403
app.use((err: any, _req: unknown, res: express.Response, _next: unknown) => {
  if (err.status) {
      return res.status(err.status).json({ error: err.message });
  } else {
    logger.error(err);
    res.status(500).json({
      error: {
        type: "proxy_error",
        message: err.message,
        stack: err.stack,
        proxy_note: `Reverse proxy encountered an internal server error.`,
      },
    });
  }
});

app.use((_req: unknown, res: express.Response) => {
  res.status(404).send(NOT_FOUND_HTML);
});

async function start() {
  logger.info("Server starting up...");

  logger.info("Checking configs and external dependencies...");
  await assertConfigIsValid();

  keyPool.init();

  if (config.gatekeeper === "user_token") {
    await initUserStore();
  }

  if (config.promptLogging) {
    logger.info("Starting prompt logging...");
    logQueue.start();
  }

  if (config.queueMode !== "none") {
    logger.info("Starting request queue...");
    startRequestQueue();
  }

  app.listen(PORT, async () => {
    logger.info({ port: PORT }, "Now listening for connections.");
    registerUncaughtExceptionHandler();
  });

  logger.info(
    { sha: process.env.COMMIT_SHA, nodeEnv: process.env.NODE_ENV },
    "Startup complete."
  );

  setInterval(async () => {
    logger.info("-!!!-ALERT-!!!- CHECKING ONLINE CONFIG. SERVER MAY HANG. -!!!-ALERT-!!!-");
    await assertConfigIsValid();
  }, INTERVAL_TIME);
}

function registerUncaughtExceptionHandler() {
  process.on("uncaughtException", (err: any) => {
    logger.error(
      { err, stack: err?.stack },
      "UNCAUGHT EXCEPTION. Please report this error trace."
    );
  });
  process.on("unhandledRejection", (err: any) => {
    logger.error(
      { err, stack: err?.stack },
      "UNCAUGHT PROMISE REJECTION. Please report this error trace."
    );
  });
}


// UNUSED!!!
async function startIntervalExecution() {
  logger.info("Starting interval execution...");
  await assertConfigIsValid(); // Initial execution

  setInterval(async () => {
    logger.info("Checking configs and external dependencies...");
    await assertConfigIsValid();
  }, INTERVAL_TIME);
}

start();
