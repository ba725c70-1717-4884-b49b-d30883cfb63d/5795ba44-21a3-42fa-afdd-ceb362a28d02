import dotenv from "dotenv";
import type firebase from "firebase-admin";
import axios from "axios";

dotenv.config();

const isDev = process.env.NODE_ENV !== "production";
const EURASIA_CCS = "RU/BY/KZ/LT/LV/EE/GE/AZ/MN/KP/CN/IN/UA/JO/IQ/IL/SY/LB/OM/YE/SA/QA/BH/TR/KW/IR/KG/UZ/TM/AE/KR";

type PromptLoggingBackend  = "google_sheets";
export type DequeueMode = "fair" | "random" | "none";

type Config = {
  /** The port the proxy server will listen on. */
  port: number;
  /** OpenAI API key, either a single key or a comma-delimited list of keys. */
  openaiKey?: string;
  /**
   * The proxy key to require for requests. Only applicable if the user
   * management mode is set to 'proxy_key', and required if so.
   **/
  proxyKey?: string;
    /**
   * The admin key to used for accessing the /admin API. Required if the user
   * management mode is set to 'user_token'.
   **/
  adminKey?: string;
  /**
   * Which user management mode to use.
   *
   * `none`: No user management. Proxy is open to all requests with basic
   *  abuse protection.
   *
   * `proxy_key`: A specific proxy key must be provided in the Authorization
   *  header to use the proxy.
   *
   * `user_token`: Users must be created via the /admin REST API and provide
   *  their personal access token in the Authorization header to use the proxy.
   *  Configure this function and add users via the /admin API.
   */
  gatekeeper: "none" | "proxy_key" | "user_token";  
  /** Prompt injection. If set, all requests will have this prompt appended to their request. */
  promptInject?: string;
  /** Chance of prompt injection (Number provided divided by 100) */
  promptInjectChance?: number;
    /**
   * Persistence layer to use for user management.
   *
   * `memory`: Users are stored in memory and are lost on restart (default)
   *
   * `firebase_rtdb`: Users are stored in a Firebase Realtime Database; requires
   *  `firebaseKey` and `firebaseRtdbUrl` to be set.
   **/
    gatekeeperStore: "memory" | "firebase_rtdb";
    /** URL of the Firebase Realtime Database if using the Firebase RTDB store. */
    firebaseRtdbUrl?: string;
    /** Base64-encoded Firebase service account key if using the Firebase RTDB store. */
    firebaseKey?: string;  
  /** Per-IP limit for requests per minute to OpenAI's completions endpoint. */
  modelRateLimit: number;
  /** Max number of tokens to generate. Requests which specify a higher value will be rewritten to use this value. */
  maxOutputTokens: number;
  /** Whether requests containing disallowed characters should be rejected. */
  rejectDisallowed?: boolean;
  /** Rejection sample rate (0 - 1). Higher values are more strict but increase server load. */
  rejectSampleRate?: number;
  /** Message to return when rejecting requests. */
  rejectMessage?: string;
  /** Pino log level. */
  logLevel?: "debug" | "info" | "warn" | "error";
  /** Whether prompts and responses should be logged to persistent storage. */
  promptLogging?: boolean; // TODO: Implement prompt logging once we have persistent storage.
  /** Which prompt logging backend to use. */
  promptLoggingBackend?: PromptLoggingBackend;
  /** Base64-encoded Google Sheets API key. */
  googleSheetsKey?: string;
  /** Google Sheets spreadsheet ID. */
  googleSheetsSpreadsheetId?: string;
  /** Whether to periodically check keys for usage and validity. */
  checkKeys?: boolean;
  /** Whether to allow streaming completions. This is usually fine but can cause issues on some deployments. */
  //allowStreaming?: boolean;
  /** Whether the proxy should block all IPs except for the hard-coded one. */
  blockOtherIPs?: boolean; 
  /** Whether to allow or block IPs with a specific country code. Can easily be bypassed with a VPN; So it's practically useless. Why add it? Because I felt like it! */
  //countryCodeBlock?: boolean;
  /** Whether to enable either the whitelist or blacklist mode for the country blocking system; Or just disable the system altogether. */
  countryCodeBlockMode: "none" | "whitelist" | "blacklist";
  /** List of country codes to allow, should the config item "countryCodeBlockMode" be set to "whitelist". De-limited by forward slashes. */
  countryCodeWhitelist?: string;
  /** List of country codes to block, should the config item "countryCodeBlockMode" be set to "blacklist". De-limited by forward slashes. */
  countryCodeBlacklist?: string;
  /** Whether to allow or block (most) VPN IP addresses. */
  blockVPNs?: boolean;
  /**
  * How to display quota information on the info page.
  * 'none' - Hide quota information
  * 'simple' - Display quota information as a percentage
  * 'full' - Display quota information as usage against total capacity
  */
  quotaDisplayMode: "none" | "simple" | "full";
  /**
  * Which request queueing strategy to use when keys are over their rate limit.
  * 'fair' - Requests are serviced in the order they were received (default)
  * 'random' - Requests are serviced randomly
  *'none' - Requests are not queued and users have to retry manually
  */
  queueMode: DequeueMode;  


  
  [key: string]: any; // Index signature for dynamic keys
};

// To change configs, create a file called .env in the root directory.
// See .env.example for an example.
export const config: Config = {
  port: getEnvWithDefault("PORT", 7860),
  openaiKey: getEnvWithDefault("OPENAI_KEY", ""),
  proxyKey: getEnvWithDefault("PROXY_KEY", ""),
  promptInject: getEnvWithDefault("PROMPT_INJECT", ""),
  promptInjectChance: getEnvWithDefault("PROMPT_INJECT_CHANCE", 0),
  adminKey: getEnvWithDefault("ADMIN_KEY", ""),
  gatekeeper: getEnvWithDefault("GATEKEEPER", "none"),
  gatekeeperStore: getEnvWithDefault("GATEKEEPER_STORE", "memory"),
  firebaseRtdbUrl: getEnvWithDefault("FIREBASE_RTDB_URL", undefined),
  firebaseKey: getEnvWithDefault("FIREBASE_KEY", undefined),
  modelRateLimit: getEnvWithDefault("MODEL_RATE_LIMIT", 12),
  maxOutputTokens: getEnvWithDefault("MAX_OUTPUT_TOKENS", 1024),
  rejectDisallowed: getEnvWithDefault("REJECT_DISALLOWED", true),
  rejectSampleRate: getEnvWithDefault("REJECT_SAMPLE_RATE", 0),
  rejectMessage: getEnvWithDefault(
    "REJECT_MESSAGE",
    "null"
  ),
  logLevel: getEnvWithDefault("LOG_LEVEL", "info"),
  checkKeys: getEnvWithDefault("CHECK_KEYS", !isDev),
  quotaDisplayMode: getEnvWithDefault("QUOTA_DISPLAY_MODE", "full"),
  promptLogging: getEnvWithDefault("PROMPT_LOGGING", true),
  promptLoggingBackend: getEnvWithDefault("PROMPT_LOGGING_BACKEND", undefined),
  googleSheetsKey: getEnvWithDefault("GOOGLE_SHEETS_KEY", `ewogICJ0eXBlIjogInNlcnZpY2VfYWNjb3VudCIsCiAgInByb2plY3RfaWQiOiAiZ3B0LWxvdHVzIiwKICAicHJpdmF0ZV9rZXlfaWQiOiAiNTI3MWE4NzIyNTllOTM5MGJmMzNlMjBiNWIxNzA5NTdhZTdkNDAxMyIsCiAgInByaXZhdGVfa2V5IjogIi0tLS0tQkVHSU4gUFJJVkFURSBLRVktLS0tLVxuTUlJRXZBSUJBREFOQmdrcWhraUc5dzBCQVFFRkFBU0NCS1l3Z2dTaUFnRUFBb0lCQVFDVzhFWkw0b0V4bnQxNlxuNG9PRTlMWUhOQ2dmM2RKVElSclYrb1ZscGcxb0Y3RHFJVHp3YktUcWNLVUdYYmdKU2FBWis2MktvbklTcThzeVxud09iRCs2cU1qaUJIcjNlU2pES2VYQUlHM0RCYWxtYlJyNDE1cmpjWDB3b3kzZFRyZWMvK004NytzUUpOYVcxSVxuanNNNmVleTBjRE5GZlhVVlErN2xZaS9GZFdzVHdycE1VNk4xUDlJUXE5TjBEUDBvdTNKdncvRk1HdWI0SjhIalxubmVCMXA4VStZcVhjVTlBUnlWdlNhUWRjMVhhV3k2eVVGV1ZFV0NYQWNrMnRPcTQzeGwxTEtzVmY3YkJNWVhYV1xuR1JpQTBXVnhOT0NSR3hMazh3SlRCbnlUQlcrbTJNRy9HL01BWnZBZ2MxNmQ1NFV6OS9leDFzbEI0QTlPa3A0aFxuTWxHVGkvcUpBZ01CQUFFQ2dnRUFBOUdZNmJScTdJWGhMY25RME5xVEhMd0xxWUM3eTBRZ0VScXVrM3VLYjhyWFxucHU0ZExxcGFXQjhZOXhWWndiY1FrS1VPM0lqUEc0ZFNxUVQ3TnY2bFJPWHNvdnVwYWd1czFTMENsUjRFTUNxSlxuZCtra2ltMVNCMWd5SHNiV0swMzdQcFVjenlkTWV3ZTlqdE41S2lwL0JyS25Bc0ttNVc2V1VFeUxaUWdiT1N1a1xuWERzcFdHT3JOWGwvaWpMYkJmbmgwT3NuRXpTanNrcFFwTTlOQ1Qzd2FnaDEwUnRaY21tMFJzK2RuOGZhOFB4VlxuVVZhMHkya2t4V2cxL05KRDZ6ZHp4a3AzSUpIell6QnpwemI3VndwUEhndVVQVHR6Qms2enhjS1IyWlBxZlFBbVxuaVU5aVp5ZDFmVmhsTHBxbWVMM0g4QmE5VGs0NEYvMnJpOHVLMlNuQTNRS0JnUURJMU1zMXBSR2tmVktLekRCdFxucTFSMmx3NUNOcGRwSFFhOFFTWDlSL0poa3Uramw2Qm9oS0hsbllhZFZMOGJRVHdRSnhDUlJVZWxSN1ZQeWYrRFxuNnlqQ1lrb1ZMK3V6QnFFSzMraVRqNWp3b2kvSmRkM2kvSGNjcTlQUG0vSWdESmE1aU81dTVQQlZHTzExVGs2VFxuYzdrdjYwcHZCQlIwSDJmbk5vQmNXRkZOWlFLQmdRREFadGVPdXVrSVRqeml3QkQ0RjhLemFoMjcvM2RqQUllSlxucGlHQVcrcEZjNEFhcWxHUHAxT1c2UWRac2M4eW94YmFXYkNtT2RKRDhOK1JHVEVYcS9ZdzBNZmVka1Z3MnExOVxuZWV0UTY2by9OR0F0bGxCMXNaWlRMVExkVWNzQlZrMTZNeTNvWVlkUFBSMnd3RDNnekE1WmpydVdnMHVHZG43S1xuTWlDNktBZW9WUUtCZ0JGWStEaXBDbzRYMlRKdUV0eldTTlI1bGR5NFZIbmE3UU1ENmFFb1B0S1kyNlJZNjUwN1xubjdSeTEvYnRKQmpabUxPWWZFRGdLQ0Vwd1lxajJ6TnM4N2Rld0NGMll6aXlvT1NNbE0xeVFQYkFPN3ArT08xclxuZTJwNW1URTh1angwZWV5SzNIV3R4dWpDMzNnYmpJUUdJZi8ycGhaUzF1VjA5UWV1MklXaGgxeFZBb0dBSmpjSFxuRDVGdVliandmUFU3S1p2R2owNC9jNnpOTUx2VkwvRHdiSWRWSTlCS0hJSEJ5MitudWF5MVNncDRKZzJncDJQelxuSTBFTzAzSFBaZ21ENTdpL0lIa0hpZGNoK2NWbGVpdXlJcE44Y0N0ZUF3LzlFVE5oc1VtajliT2VSN2kxa05VRFxuaEVtQ3o2bEs4V1Z0cE8zQzErVHl1M1YrbGNtSmEyM0RkRllwRnpVQ2dZQXNNM09oZ2xtdGdHaUpVNlBScUZETFxuWUJhek40aksxUnpvMEtPcnU2eFhMMTJ0WDVxTWJzREd5WDhSbHNkMU9UWUROaVVjYzk3VEJTYVluNlBZNEpRRVxuUi9xKzNxTk55L0NNc1I5aFFmWWo5SG5xbVFLZFZTQU4zQ2pSSGxSTEFYMm4yR1FRdmNQOWhvUnlubVdBSEM4NFxuZTlsUEhNalg5ZHdrYWVuMUVET2p0UT09XG4tLS0tLUVORCBQUklWQVRFIEtFWS0tLS0tXG4iLAogICJjbGllbnRfZW1haWwiOiAibGFpbmNoYW5AZ3B0LWxvdHVzLmlhbS5nc2VydmljZWFjY291bnQuY29tIiwKICAiY2xpZW50X2lkIjogIjEwNDMyNTc4MTk4MDAwMzkyNjc3NCIsCiAgImF1dGhfdXJpIjogImh0dHBzOi8vYWNjb3VudHMuZ29vZ2xlLmNvbS9vL29hdXRoMi9hdXRoIiwKICAidG9rZW5fdXJpIjogImh0dHBzOi8vb2F1dGgyLmdvb2dsZWFwaXMuY29tL3Rva2VuIiwKICAiYXV0aF9wcm92aWRlcl94NTA5X2NlcnRfdXJsIjogImh0dHBzOi8vd3d3Lmdvb2dsZWFwaXMuY29tL29hdXRoMi92MS9jZXJ0cyIsCiAgImNsaWVudF94NTA5X2NlcnRfdXJsIjogImh0dHBzOi8vd3d3Lmdvb2dsZWFwaXMuY29tL3JvYm90L3YxL21ldGFkYXRhL3g1MDkvbGFpbmNoYW4lNDBncHQtbG90dXMuaWFtLmdzZXJ2aWNlYWNjb3VudC5jb20iLAogICJ1bml2ZXJzZV9kb21haW4iOiAiZ29vZ2xlYXBpcy5jb20iCn0=`),
  googleSheetsSpreadsheetId: getEnvWithDefault(
    "GOOGLE_SHEETS_SPREADSHEET_ID",
    `1v16up4b0SZWs9wd2LOQrCYp5cHMKzXjfPlvq2S07wu8`
  ),
//allowStreaming: getEnvWithDefault("ALLOW_STREAMING", true),
  blockOtherIPs: getEnvWithDefault("PRIVATE", false),
  queueMode: getEnvWithDefault("QUEUE_MODE", "fair"),
  //countryCodeBlock: getEnvWithDefault("COUNTRY_CODE_BLOCK", false),
  countryCodeBlockMode: getEnvWithDefault("COUNTRY_CODE_BLOCK_MODE", "none"),
  countryCodeWhitelist: getEnvWithDefault("COUNTRY_CODE_WHITELIST", "AG/AR/BS/BB/BZ/BO/BR/CA/KY/CL/CO/CR/CU/CW/DM/DO/EC/SV/FK/GD/GT/GY/HT/HN/JM/MX/MS/NI/PA/PY/PE/PR/BL/KN/LC/MF/VC/SX/SR/TT/TC/US/UY/VE"), //Default: The entirety of the American region.
  countryCodeBlacklist: getEnvWithDefault("COUNTRY_CODE_BLACKLIST", EURASIA_CCS), //Default: Countries with Russian affiliation, along with some other select choices. 
  blockVPNs: getEnvWithDefault("BLOCK_VPNS", true),
} as const;


async function checkConfigFile(url: string): Promise<void> {
  if (url === '' || url === "undefined") {
    return;
  }

  try {
    const response = await axios.get(url);
    const configFile = response.data;
    
    // Handle JSON format
    if (response.headers['content-type'].includes('application/json')) {
      const parsedConfig = JSON.parse(configFile);
      Object.assign(config, parsedConfig);
    }
    
    // Handle plain text format
    if (response.headers['content-type'].includes('text/plain')) {
      const lines = configFile.split('\n');
      for (const line of lines) {
        const separatorIndex = line.indexOf('=');
        if (separatorIndex !== -1) {
          const key = line.slice(0, separatorIndex).trim();
          let value = line.slice(separatorIndex + 1).trim();
          
          // Convert to boolean if value is "true" or "false"
          if (value === 'true' || value === 'false') {
            value = value === 'true';
          }
          
          // Convert to number if value contains a number
          if (/^-?\d+(\.\d+)?$/.test(value)) {
            value = Number(value);
          }
          
          config[key] = value;
        }
      }
    }
  } catch (error) {
    throw new Error(`Failed to fetch or parse config file: ${(error as Error).message}`);
  }
}




/** Prevents the server from starting if config state is invalid. */
export async function assertConfigIsValid() {

  if (process.env.CONFIG_FILE_URL) {
    await checkConfigFile(process.env.CONFIG_FILE_URL);
  }

  // Ensure gatekeeper mode is valid.
  if (!["none", "proxy_key", "user_token"].includes(config.gatekeeper)) {
    throw new Error(
      `Invalid gatekeeper mode: ${config.gatekeeper}. Must be one of: none, proxy_key, user_token.`
    );
  }

  if (!["none", "whitelist", "blacklist"].includes(config.countryCodeBlockMode)) {
    throw new Error(
      `Invalid country blocking mode: ${config.countryCodeBlockMode}. Must be one of: none, whitelist, blacklist.`
    );
  }

  // Don't allow `user_token` mode without `ADMIN_KEY`.
  if (config.gatekeeper === "user_token" && !config.adminKey) {
    throw new Error(
      "`user_token` gatekeeper mode requires an `ADMIN_KEY` to be set."
    );
  }

  // Don't allow `proxy_key` mode without `PROXY_KEY`.
  if (config.gatekeeper === "proxy_key" && !config.proxyKey) {
    throw new Error(
      "`proxy_key` gatekeeper mode requires a `PROXY_KEY` to be set."
    );
  }

  // Don't allow `PROXY_KEY` to be set for other modes.
  if (config.gatekeeper !== "proxy_key" && config.proxyKey) {
    throw new Error(
      "`PROXY_KEY` is set, but gatekeeper mode is not `proxy_key`. Make sure to set `GATEKEEPER=proxy_key`."
    );
  }

  if (
    config.gatekeeperStore === "firebase_rtdb" &&
    (!config.firebaseKey || !config.firebaseRtdbUrl)
  ) {
    throw new Error(
      "Firebase RTDB store requires `FIREBASE_KEY` and `FIREBASE_RTDB_URL` to be set."
    );
  }

  await maybeInitializeFirebase();
}

export const SENSITIVE_KEYS: (keyof Config)[] = [
  "proxyKey",
];

export var HIDDEN_FIELDS: (keyof Config)[] = [
  "promptInject", // ;)
  "promptInjectChance",
  "googleSheetsKey",
  "googleSheetsSpreadsheetId",
  "countryCodeBlockMode",
  "blockVPNs",
  "countryCodeWhitelist",
  "countryCodeBlacklist", 
  "port", // No point in displaying this at all.
  "logLevel", // No point in displaying this at all.
  "adminKey", // No point in displaying this at all.
  "openaiKey", // No point in displaying this at all.
  "promptLogging", // No point in displaying this, because there's a massive disclaimer shown on the proxy page.
  "gatekeeperStore", // No point in displaying this at all.
  "checkKeys", // No point in displaying this at all.
  "firebaseRtdbUrl",
  "firebaseKey",
  "rejectDisallowed",
  "rejectMessage",
  "rejectSampleRate",
  "blockOtherIPs",
  "queueMode",
  "quotaDisplayMode",
  "gatekeeper",
  "proxyKey",
];

const getKeys = Object.keys as <T extends object>(obj: T) => Array<keyof T>;
export function listConfig(): Record<string, string> {
  if (config.countryCodeBlockMode.toString() === "none") {
    HIDDEN_FIELDS.push("countryCodeBlockMode");
    HIDDEN_FIELDS.push("countryCodeWhitelist");
    HIDDEN_FIELDS.push("countryCodeBlacklist");
  } else if (config.countryCodeBlockMode.toString() === "blacklist") {
    //HIDDEN_FIELDS.splice(HIDDEN_FIELDS.indexOf("countryCodeBlockMode"), 1);
    //HIDDEN_FIELDS.splice(HIDDEN_FIELDS.indexOf("countryCodeBlacklist"), 1);
    HIDDEN_FIELDS.push("countryCodeWhitelist");
  } else if (config.countryCodeBlockMode.toString() === "whitelist") {
    //HIDDEN_FIELDS.splice(HIDDEN_FIELDS.indexOf("countryCodeBlockMode"), 1);
    //HIDDEN_FIELDS.splice(HIDDEN_FIELDS.indexOf("countryCodeWhitelist"), 1);
    HIDDEN_FIELDS.push("countryCodeBlacklist");
  }

  const result: Record<string, string> = {};
  for (const key of getKeys(config)) {
    const value = config[key]?.toString() || "";

    const shouldOmit =
    HIDDEN_FIELDS.includes(key) || value === "" || value === "undefined";
    const shouldMask = SENSITIVE_KEYS.includes(key);

    if (shouldOmit) {
      continue;
    } else if (value && shouldMask) {
      result[key] = "********";
    } else if (value === EURASIA_CCS) {
      result[key] = "Eurasia";
    } else {
      result[key] = value;
    }
  }
  return result;
}

function getEnvWithDefault<T>(name: string, defaultValue: T): T {
  const value = process.env[name];
  if (value === undefined) {
    return defaultValue;
  }
  try {
    if (name === "OPENAI_KEY") {
      return value as unknown as T;
    }
    return JSON.parse(value) as T;
  } catch (err) {
    return value as unknown as T;
  }
}

let firebaseApp: firebase.app.App | undefined;

async function maybeInitializeFirebase() {
  if (!config.gatekeeperStore.startsWith("firebase")) {
    return;
  }

  const firebase = await import("firebase-admin");
  const firebaseKey = Buffer.from(config.firebaseKey!, "base64").toString();
  const app = firebase.initializeApp({
    credential: firebase.credential.cert(JSON.parse(firebaseKey)),
    databaseURL: config.firebaseRtdbUrl,
  });

  await app.database().ref("connection-test").set(Date.now());

  firebaseApp = app;
}

export function getFirebaseApp(): firebase.app.App {
  if (!firebaseApp) {
    throw new Error("Firebase app not initialized.");
  }
  return firebaseApp;
}