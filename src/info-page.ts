import fs from "fs";
import { Request, Response } from "express";
import showdown from "showdown";
import { config, listConfig } from "./config";
import { keyPool } from "./key-management";
import { getUniqueIps } from "./proxy/rate-limit";
import { getAverageWaitTime, getQueueLength } from "./proxy/queue";

const INFO_PAGE_TTL = 5000;
let infoPageHtml: string | undefined;
let infoPageLastUpdated = 0;

export const handleInfoPage = (req: Request, res: Response) => {
  if (infoPageLastUpdated + INFO_PAGE_TTL > Date.now()) {
    res.send(infoPageHtml);
    return;
  }

  // Huggingface puts spaces behind some cloudflare ssl proxy, so `req.protocol` is `http` but the correct URL is actually `https`
  const host = req.get("host");
  const isHuggingface = host?.includes("hf.space");
  const protocol = isHuggingface ? "https" : req.protocol;
  res.send(cacheInfoPageHtml(protocol + "://" + host));};

function cacheInfoPageHtml(host: string) {
  const keys = keyPool.list();
  let keyInfo: Record<string, any> = { all: keys.length };

  if (keyPool.anyUnchecked()) {
    const uncheckedKeys = keys.filter((k) => !k.lastChecked);
    keyInfo = {
      //...keyInfo,
      all: `?`,
      active: keys.filter((k) => !k.isDisabled).length,
      status: `Still checking ${uncheckedKeys.length} keys...`,
    };
  } else if (config.checkKeys) {
    const trialKeys = keys.filter((k) => k.isTrial);
    const turboKeys = keys.filter((k) => !k.isGpt4 && !k.isDisabled);
    const gpt4Keys = keys.filter((k) => k.isGpt4 && !k.isDisabled);

    const quota: Record<string, string> = { turbo: "", gpt4: "" };
    const hasGpt4 = keys.some((k) => k.isGpt4);

    if (config.quotaDisplayMode === "full") {
      quota.turbo = `${keyPool.usageInUsd()} (${Math.round(
        keyPool.remainingQuota() * 100
      )}% remaining)`;
      quota.gpt4 = `${keyPool.usageInUsd(true)} (${Math.round(
        keyPool.remainingQuota(true) * 100
      )}% remaining)`;
    } else {
      quota.turbo = `${Math.round(keyPool.remainingQuota() * 100)}%`;
      //quota.gpt4 = `${Math.round(keyPool.remainingQuota(true) * 100)}%`;
      quota.gpt4 = `DISABLED`;
    }

    if (true === true || !hasGpt4) {
      delete quota.gpt4;
    }

    keyInfo = {
      //...keyInfo,
      all: `?`,
      //trial: trialKeys.length,
      active: {
        turbo: turboKeys.length,
        //...(hasGpt4 ? { gpt4: gpt4Keys.length } : {}),
      },
      ...(config.quotaDisplayMode !== "none" ? { quota: quota } : {}),
    };
  }

  const info = {
    uptime: process.uptime(),
    timestamp: Date.now(),
    endpoints: {
      kobold: host,
      openai: host + "/proxy/openai",
    },
    proompts: keys.reduce((acc, k) => acc + k.promptCount, 0),
    ...(config.modelRateLimit ? { proomptersNow: getUniqueIps() } : {}),
    ...getQueueInformation(),
    // keys: keyInfo,
    keyInfo,
    config: listConfig(),
    sha: process.env.COMMIT_SHA?.slice(0, 7) || "null",
  };
  
  const title = "lainchan proxy";

  const pageBody = `<!DOCTYPE html>
  <html lang="en">
    <head>
      <meta charset="utf-8" />
      <meta http-equiv="X-Frame-Options" content="DENY">
      <meta name="robots" content="none"/>
      <meta name="googlebot" content="none"/>
      <meta name="googlebot-news" content="none"/>
      <meta name="slurp" content="none"/>
      <meta name="msnbot" content="none"/>
      <title>${title}</title>
      <style>
        body {
          font-family: 'Courier New', Courier, monospace;
          background-color: black;
          color: #f0f0f0;
          overflow: auto;
          padding: 0.618em;
        }     
        #img-container {
          position: fixed;
          bottom: 0;
          right: 0;
          padding: 16px;
        }
        
        #img-container img {
          width: 256px; /* change this value to scale down the image */
          height: auto;
        }
      </style>
    </head>

    <body>
      <div id="img-container">
        <img src="https://lainchan.org/static/lainchan_header_console.gif" alt="love lain">
      </div>
      ${infoPageHeaderHtml}
      <hr />
      <h1>Latest News:<h4/>
	  <p>5/28/2023 | Just started the proxy.<p/>
      <hr />
      <h2>Service Info:</h2>
      <pre id="service_info">${JSON.stringify(info, null, 2)}</pre>
      <hr />
	  <h3 style="text-align:center;">CYBERPUNK IS HARD<h4/>
      <h4 style="text-align:center;">Pour out the Soykaf, coom, and enjoy!<h4/>
    </body>
  </html>`;

  infoPageHtml = pageBody;
  infoPageLastUpdated = Date.now();

  return pageBody;
}

const infoPageHeaderHtml = buildInfoPageHeader(new showdown.Converter());

/**
 * If the server operator provides a `greeting.md` file, it will be included in
 * the rendered info page.
 **/
function buildInfoPageHeader(converter: showdown.Converter) {
  const genericInfoPage = fs.readFileSync("info-page.md", "utf8");
  const customGreeting = fs.existsSync("greeting.md")
    ? fs.readFileSync("greeting.md", "utf8")
    : null;

  let infoBody = genericInfoPage;
  if (false === true && config.promptLogging) {
    infoBody += `\n#### Prompt logging is enabled!
The server operator has enabled prompt logging. Requests sent to this proxy are saved as logs.

The logs contain your:
1. Prompt
2. Any AI responses you recieve
3. IP address

These logs will not be shared, and they are not public. They are for the server operator's eyes only.

This is to prevent misuse and key-draining.

**If you are uncomfortable with this, DO NOT send prompts to this proxy!** `;
  }

  if (false === true && config.queueMode !== "none") {
    const friendlyWaitTime = getQueueInformation().estimatedWaitTime;
    infoBody += `\n### Estimated Queue Wait Time: ${friendlyWaitTime}
Requests are queued. If the AI is busy, your prompt will be queued and processed when a slot is available.

You can check wait times below. **Be sure to enable streaming in your client, or your request will likely time out.**`;
  }

  if (customGreeting) {
    infoBody += `\n## Server Greeting\n
${customGreeting}`;
  }
  return converter.makeHtml(infoBody);
}

function getQueueInformation() {
  if (config.queueMode === "none") {
    return {};
  }
  const waitMs = getAverageWaitTime();
  const waitTime =
    waitMs < 60000
      ? `${Math.round(waitMs / 1000)} seconds`
      : `${Math.round(waitMs / 60000)} minutes`;
  return {
    proomptersWaiting: getQueueLength(),
    estimatedWaitTime: waitMs > 3000 ? waitTime : "no wait",
  };
}