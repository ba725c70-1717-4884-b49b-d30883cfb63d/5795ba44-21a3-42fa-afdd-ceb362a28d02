import { config } from "../../../config";
import { logger } from "../../../logger";
import type { ExpressHttpProxyReqCallback } from ".";

const DISALLOWED_REGEX =
  /[^\x00-\xFF]/;

// Our shitty free-tier VMs will fall over if we test every single character in
// each 15k character request ten times a second. So we'll just sample 20% of
// the characters and hope that's enough.
const containsDisallowedCharacters = (text: string) => {
  const sampleSize = Math.ceil(text.length * (config.rejectSampleRate || 0.2));
  const sample = text
    .split("")
    .sort(() => 0.5 - Math.random())
    .slice(0, sampleSize)
    .join("");
  return DISALLOWED_REGEX.test(sample);
};

/** Block requests containing too many disallowed characters. */
export const languageFilter: ExpressHttpProxyReqCallback = (_proxyReq, req) => {
  if (!config.rejectDisallowed) {
    return;
  }

  if (req.method === "POST" && req.body?.messages) {
    const combinedText = req.body.messages
      .map((m: { role: string; content: string }) => m.content)
      .join(",");
    if (containsDisallowedCharacters(combinedText)) {
      logger.warn(`Blocked request containing bad characters`);
      _proxyReq.destroy(new Error(config.rejectMessage));
    }
  }
};
