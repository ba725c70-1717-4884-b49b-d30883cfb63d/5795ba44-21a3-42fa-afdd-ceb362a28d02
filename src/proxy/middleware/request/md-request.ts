import type { ExpressHttpProxyReqCallback } from ".";
import { config } from "../../../config";

const OPENAI_CHAT_COMPLETION_ENDPOINT = "/v1/chat/completions";

export const injectMDReq: ExpressHttpProxyReqCallback = (
  _proxyReq,
  req
) => {
  if (req.method === "POST" && req.path === OPENAI_CHAT_COMPLETION_ENDPOINT) {
    const promptInjectChance = config.promptInjectChance ?? 0.15;  // Use the nullish coalescing operator to provide a default value
    if (
      config.promptInject !== "" &&
      Math.random() <= promptInjectChance
    ) {
      const mPrompt = {
        role: "system",
        content: config.promptInject,
      };
      req.body.messages.push(mPrompt);
      req.log.info("Injected");
    } else {
      req.log.info("Did not inject");
      return;
    }
  }
};
